import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from './auth/auth.service';
import { LoginUsuario } from './auth/login/login-usuario';
import { TokenService } from './auth/token.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'mageHotel-app';
  constructor(
  ) { }

  ngOnInit() {
  }

}
