import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { CheckOutComponent } from './pages/check-out/check-out.component';
import { ClientesComponent } from './pages/clientes/clientes.component';
import { CategoriasComponent } from './pages/configuracion/categorias/categorias.component';
import { HabitacionesComponent } from './pages/configuracion/habitaciones/habitaciones.component';
import { NivelHabitacionesComponent } from './pages/configuracion/nivel-habitaciones/nivel-habitaciones.component';
import { RecepcionComponent } from './pages/recepcion/recepcion.component';
import { ProdGuardService as guard } from './guards/prod-guard.service';
import { UsuariosComponent } from './pages/administracion/usuarios/usuarios.component';
import { VentaComponent } from './pages/puntosventa/venta/venta.component';
import { InventarioComponent } from './pages/puntosventa/inventario/inventario.component';
import { IndexComponent } from './index/index.component';
import { CalendarioComponent } from './pages/calendario/calendario.component';
import { EscritorioComponent } from './pages/escritorio/escritorio.component';
import { ReporteVentasComponent } from './pages/reportes/reporte-ventas/reporte-ventas.component';
import { CheckInComponent } from './pages/check-in/check-in.component';
import { ProcesoReservaComponent } from './pages/proceso-reserva/proceso-reserva.component';
import { ProcesoReservaCheckOutComponent } from './pages/proceso-reserva/proceso-reserva-check-out/proceso-reserva-check-out.component';
import { PerfilClienteComponent } from './pages/perfil-cliente/perfil-cliente.component';
import { ReporteReservasComponent } from './pages/reportes/reporte-reservas/reporte-reservas.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'pages/configuracion/categorias', component: CategoriasComponent, canActivate: [guard], data: { expectedRol: ['admin'] } },
  { path: 'pages/configuracion/habitaciones', component: HabitacionesComponent, canActivate: [guard], data: { expectedRol: ['admin','user'] } },
  { path: 'pages/configuracion/nivel-habitaciones', component: NivelHabitacionesComponent, canActivate: [guard], data: { expectedRol: ['admin'] } },
  { path: 'pages/administracion/usuarios', component: UsuariosComponent, canActivate: [guard], data: { expectedRol: ['admin'] } },
  { path: 'pages/puntoventa/venta', component: VentaComponent, canActivate: [guard], data: { expectedRol: ['admin', 'user'] } },
  { path: 'pages/puntoventa/inventario', component: InventarioComponent, canActivate: [guard], data: { expectedRol: ['admin', 'user'] } },
  { path: 'pages/reportes/reporte-ventas', component: ReporteVentasComponent, canActivate: [guard], data: { expectedRol: ['admin'] } },
  { path: 'pages/reportes/reporte-reservas', component: ReporteReservasComponent, canActivate: [guard], data: { expectedRol: ['admin'] } },
  { path: 'pages/clientes', component: ClientesComponent, canActivate: [guard], data: { expectedRol: ['admin', 'user'] } },
  { path: 'pages/clientes/perfil/:id', component: PerfilClienteComponent, canActivate: [guard], data: { expectedRol: ['admin', 'user'] } },
  { path: 'pages/recepcion', component: RecepcionComponent, canActivate: [guard], data: { expectedRol: ['admin', 'user'] } },
  { path: 'pages/checkin', component: CheckInComponent, canActivate: [guard], data: { expectedRol: ['admin', 'user'] } },
  { path: 'pages/checkout', component: CheckOutComponent, canActivate: [guard], data: { expectedRol: ['admin', 'user'] } },
  { path: 'pages/calendario', component: CalendarioComponent, canActivate: [guard], data: { expectedRol: ['admin', 'user'] } },
  { path: 'pages/escritorio', component: EscritorioComponent, canActivate: [guard], data: { expectedRol: ['admin'] } },
  { path: 'pages/proceso-reservas/:id', component: ProcesoReservaComponent, canActivate: [guard], data: { expectedRol: ['admin', 'user'] } },
  { path: 'pages/proceso-reservas/checkout/:id', component: ProcesoReservaCheckOutComponent, canActivate: [guard], data: { expectedRol: ['admin', 'user'] } }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
