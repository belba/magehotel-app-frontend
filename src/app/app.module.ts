import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CategoriasComponent } from './pages/configuracion/categorias/categorias.component';
import { CategoriaService } from './pages/configuracion/categorias/categoria.service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { HabitacionesComponent } from './pages/configuracion/habitaciones/habitaciones.component';
import { NivelHabitacionesComponent } from './pages/configuracion/nivel-habitaciones/nivel-habitaciones.component';
import { ClientesComponent } from './pages/clientes/clientes.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ClienteService } from './pages/clientes/cliente.service';
import { PisoService } from './pages/configuracion/nivel-habitaciones/nivel-habitaciones.service';
import { HabitacionService } from './pages/configuracion/habitaciones/habitacion.service';
import { NavbarComponent } from './pages/navbar/navbar.component';
import { RecepcionComponent } from './pages/recepcion/recepcion.component';
import { CheckOutComponent } from './pages/check-out/check-out.component';
import { ToastrModule } from 'ngx-toastr';
import { LoginComponent } from './auth/login/login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ProdInterceptorService } from './auth/interceptors/prod-interceptor.service';
import { AuthService } from './auth/auth.service';
import { TokenService } from './auth/token.service';
import { ProdGuardService } from './guards/prod-guard.service';
import { IndexComponent } from './index/index.component';
import { UsuariosComponent } from './pages/administracion/usuarios/usuarios.component';
import { InventarioComponent } from './pages/puntosventa/inventario/inventario.component';
import { VentaComponent } from './pages/puntosventa/venta/venta.component';
import { EscritorioComponent } from './pages/escritorio/escritorio.component';
import { CalendarioComponent } from './pages/calendario/calendario.component';
import { ReporteVentasComponent } from './pages/reportes/reporte-ventas/reporte-ventas.component';
import { CheckInComponent } from './pages/check-in/check-in.component';
import { CheckInService } from './pages/check-in/check-in.service';
import { ProcesoReservaComponent } from './pages/proceso-reserva/proceso-reserva.component';
import { ProcesoReservaCheckOutComponent } from './pages/proceso-reserva/proceso-reserva-check-out/proceso-reserva-check-out.component';
import { UsuarioService } from './pages/administracion/usuarios/usuario.service';
import { PerfilClienteComponent } from './pages/perfil-cliente/perfil-cliente.component';
import { ReporteReservasComponent } from './pages/reportes/reporte-reservas/reporte-reservas.component';

@NgModule({
  declarations: [
    AppComponent,
    CategoriasComponent,
    HabitacionesComponent,
    NivelHabitacionesComponent,
    ClientesComponent,
    NavbarComponent,
    RecepcionComponent,
    CheckOutComponent,
    LoginComponent,
    IndexComponent,
    UsuariosComponent,
    InventarioComponent,
    VentaComponent,
    EscritorioComponent,
    CalendarioComponent,
    ReporteVentasComponent,
    CheckInComponent,
    ProcesoReservaComponent,
    ProcesoReservaCheckOutComponent,
    PerfilClienteComponent,
    ReporteReservasComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgbModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()

  ],
  providers: [CategoriaService,
    PisoService,
    ClienteService,
    HabitacionService,
    CheckInService,
    ProdInterceptorService,AuthService,
    ProdInterceptorService,
    AuthService,
  UsuarioService],
  bootstrap: [AppComponent]
})
export class AppModule { }
