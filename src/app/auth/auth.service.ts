import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoginUsuario } from './login/login-usuario';
import { JwtDTO } from './jwt-dto';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  authURL = 'http://localhost:8181/api/auth/';

  constructor(private httpClient: HttpClient) { }

  public login(loginUsuario: LoginUsuario): Observable<JwtDTO> {
    return this.httpClient.post<JwtDTO>(this.authURL + 'login',{
      nombreUsuario: loginUsuario.nombreUsuario,
      passowrd: loginUsuario.password
    }, );
  }
}
