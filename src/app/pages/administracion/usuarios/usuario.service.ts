import { HttpClient } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Usuario } from './Usuario';
import { NuevoUsuario } from './Usuario';

@Injectable()
export class UsuarioService{
  private urlEndPoint: string = 'http://localhost:8181/api/auth/usuarios';

  constructor(private http: HttpClient){}

  getUsuarios(): Observable<Usuario[]>{

   return this.http.get(this.urlEndPoint).pipe(
      map((response: any) => {
        (response as Usuario[]).map(usuario => {
          return usuario;
        });
        return response;
      }));
  }


  getUsuario(id): Observable<Usuario>{
    return this.http.get<Usuario>(`${this.urlEndPoint}/${id}`);
  }

  crearUsuario(nuevoUsuario: NuevoUsuario): Observable<NuevoUsuario>{
    return this.http.post(this.urlEndPoint, nuevoUsuario)
    .pipe(
      map((response: any ) => response.usuario as NuevoUsuario)
    );
  }

  updateUsuario(usuario: Usuario): Observable<any> {
    return this.http.put<any>(`${this.urlEndPoint}/${usuario.id}`, usuario);
  }

  eliminarUsuario(id: number): Observable<Usuario>{
    return this.http.delete<Usuario>(`${this.urlEndPoint}/${id}`);
  }

}
