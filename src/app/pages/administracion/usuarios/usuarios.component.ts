import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalConfig, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { NuevoUsuario, Usuario, Rol } from './Usuario';
import { UsuarioService } from './usuario.service';
@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.sass']
})
export class UsuariosComponent implements OnInit {

  modal : NgbModalRef;
  usuarios: Usuario[];
  nuevoUsuario: NuevoUsuario = new NuevoUsuario();
  usuario: Usuario = new Usuario();
  roles: Rol[];

  estados =[
    {id: 2, nombre: "Gerente", jq: "admin"},
    {id: 1, nombre: "Recepcionista", jq: "user"},
  ]
  constructor(private usuarioService: UsuarioService,
   // private pisoService: PisoService,
    //private categoriaService: CategoriaService,
    private modalService: NgbModal,
    config: NgbModalConfig) {
    config.backdrop = 'static';
    config.keyboard = false;
   }

  ngOnInit(): void {
    this.usuarioService.getUsuarios().subscribe(response => {
      this.usuarios = response as Usuario[];
    console.log(this.usuarios);

  });
  }

  open(content, id) {
    this.modal = this.modalService.open(content, { centered: true, backdropClass: 'light-blue-backdrop' })
    this.modal.result.then((e) => {
    });

    /*this.pisoService.getPisos().subscribe(pisos => this.pisos = pisos);
    this.categoriaService.getCategorias().subscribe(categorias => this.categorias = categorias);*/

    if(id){
      this.usuarioService.getUsuario(id).subscribe( response => {
        this.usuario = response as Usuario;
        console.log(response);
        this.usuario.password="";

      });
    }else{
      this.usuario = new Usuario();
    }
  }

  compararRol(o1: Rol, o2: Rol): boolean {
    if (o1 === undefined && o2 === undefined) {
      return true;
    }

    return o1 === null || o2 === null || o1 === undefined || o2 === undefined ? false : o1.id === o2.id;
  }
/*
  compararCategoria(o1: Categoria, o2: Categoria): boolean {
    if (o1 === undefined && o2 === undefined) {
      return true;
    }

    return o1 === null || o2 === null || o1 === undefined || o2 === undefined ? false : o1.id === o2.id;
  }
*/
  cerrar() {
    this.modal.close();
  }

  crear(): void{
    console.log(this.usuario);
    this.usuarioService.crearUsuario(this.usuario).subscribe(
      usuario => {
        this.ngOnInit();
        this.modal.close();
      });
  }

  update(): void{
    this.usuarioService.updateUsuario(this.usuario).subscribe(
      usuario => {
        this.ngOnInit();
        this.modal.close();
      }
    )
  }

  eliminar(usuario: Usuario): void{
    this.usuarioService.eliminarUsuario(usuario.id).subscribe(
      () => {
        this.usuarios = this.usuarios.filter(hab => hab !== usuario)
      }
    );
  }

}
