export class Usuario{
  id: number;
  nombre: String;
  email: String;
  nombreUsuario: String;
  password: String;
  roles: String;
}

export class NuevoUsuario{
  id: number;
  nombre: String;
  email: String;
  nombreUsuario: String;
  password: String;
  roles: String;
}

export class Rol{
  id: number;
  rolNombre: String;
}
