import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcesoReservaCheckOutComponent } from './proceso-reserva-check-out.component';

describe('ProcesoReservaCheckOutComponent', () => {
  let component: ProcesoReservaCheckOutComponent;
  let fixture: ComponentFixture<ProcesoReservaCheckOutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcesoReservaCheckOutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcesoReservaCheckOutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
