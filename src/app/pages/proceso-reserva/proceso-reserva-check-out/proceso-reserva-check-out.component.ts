import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { CheckInService } from '../../check-in/check-in.service';
import { Reserva } from '../../check-in/reserva';
import { HabitacionService } from '../../configuracion/habitaciones/habitacion.service';

@Component({
  selector: 'app-proceso-reserva-check-out',
  templateUrl: './proceso-reserva-check-out.component.html',
  styleUrls: ['./proceso-reserva-check-out.component.sass']
})
export class ProcesoReservaCheckOutComponent implements OnInit {
  reservas: Reserva[];
  reserva: Reserva;
  preciototal: number;
  constructor(private checkInService: CheckInService, private activatedRoute: ActivatedRoute, private router: Router, private habitacionService: HabitacionService) { }

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe(params => {
      let id = +params.get('id');
      if (id) {
        this.checkInService.getReserva(id).subscribe( response => {
          this.reserva = response as Reserva;
        });
      }
    });

    this.checkInService.getReservas().subscribe(response => {
      this.reservas = response as Reserva[];
    })
  }

  calculoPrecioTotal(): number {
    var fecha1 = moment(this.reserva.fecha_inicio);
    var fecha2 = moment(this.reserva.fecha_final);
    let numDias = fecha2.diff(fecha1, 'days');

    let precioPorPersona = numDias * this.reserva.habitacion.categoria.precio;

    return precioPorPersona;

  }

  fechaInicio(data): string {
    return moment(data, "YYYY-MM-DD").format("DD/MM/YYYY").toLocaleString();
  }

  fechaFin(data): string {
    return moment(data, "YYYY-MM-DD").format("DD/MM/YYYY").toLocaleString();
  }

  cerrarReserva(){
    this.reserva.estado = 2
    this.habitacionService.getHabitacion(this.reserva.habitacion.id).subscribe( response => {
      response.estado = 3;
      console.log(response);

      this.habitacionService.updateHabitacion(response).subscribe(
      )
    });
    this.checkInService.updateReserva(this.reserva).subscribe(
      reserva => {
        this.router.navigate(['/pages/checkout']);
      }
    )
  }

}
