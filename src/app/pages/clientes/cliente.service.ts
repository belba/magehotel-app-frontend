import { Injectable } from "@angular/core";
import { HttpClient, HttpRequest, HttpEvent } from '@angular/common/http';
import { map, catchError, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Cliente } from './clientes';

@Injectable()
export class ClienteService{
  private urlEndPoint: string = 'http://localhost:8181/api/clientes';
  productoURL = 'http://localhost:8181/api/';
  constructor(private http: HttpClient){}

  public getClientes(): Observable<Cliente[]> {
    return this.http.get<Cliente[]>(this.productoURL + 'clientes');
  }

  getCliente(id): Observable<Cliente>{
    return this.http.get<Cliente>(`${this.urlEndPoint}/${id}`);
  }

  crearCliente(cliente: Cliente): Observable<Cliente> {
    return this.http.post(this.urlEndPoint, cliente)
    .pipe(
      map((response: any ) => response.cliente as Cliente)
    );
  }

  eliminarCliente(id: number): Observable<Cliente>{
    return this.http.delete<Cliente>(`${this.urlEndPoint}/${id}`);
  }

  updateCliente(cliente: Cliente): Observable<any> {
    return this.http.put<any>(`${this.urlEndPoint}/${cliente.id}`, cliente);
  }

}
