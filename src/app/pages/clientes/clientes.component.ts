import { Component, OnInit } from '@angular/core';
import { Cliente } from './clientes';
import { ClienteService } from './cliente.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal, NgbModalConfig, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { TokenService } from 'src/app/auth/token.service';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.sass']
})
export class ClientesComponent implements OnInit {

  modal : NgbModalRef;
  clientes: Cliente[];
  cliente: Cliente = new Cliente();
  roles: string[];
  isAdmin = false;

  constructor(private clienteService: ClienteService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    config: NgbModalConfig,
    private modalService: NgbModal,
    private tokenService: TokenService) {
    config.backdrop = 'static';
    config.keyboard = false;

   }

  ngOnInit(): void {
    this.cargarClientes();
    this.roles = this.tokenService.getAuthorities();
    this.roles.forEach(rol => {
      if (rol === 'ROLE_ADMIN') {
        this.isAdmin = true;
      }
    });
  }

  cargarClientes(): void {
    this.clienteService.getClientes().subscribe(
      data => {
        this.clientes = data;
      },
      err => {
        console.log(err);
      }
    );
  }

  open(content, id) {
    this.modal = this.modalService.open(content, { centered: true, backdropClass: 'light-blue-backdrop' })
    this.modal.result.then((e) => {
    });

    if(id){
      this.clienteService.getCliente(id).subscribe((cliente) => this.cliente = cliente);
    }else{
      this.cliente = new Cliente();
    }
  }

  eliminar(cliente: Cliente): void{
    this.clienteService.eliminarCliente(cliente.id).subscribe(
      () => {
        this.clientes = this.clientes.filter(cat => cat !== cliente)
      }
    );
  }

  cerrar() {
    this.modal.close();
  }

  crear(): void{
    this.clienteService.crearCliente(this.cliente).subscribe(
      cliente => {
        this.ngOnInit();
        this.modal.close();
      });
  }

  update(): void{
    this.clienteService.updateCliente(this.cliente).subscribe(
      cliente => {
        this.ngOnInit();
        this.modal.close();
      }
    )
  }

}
