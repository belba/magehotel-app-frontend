import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TokenService } from '../../auth/token.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.sass']
})
export class NavbarComponent implements OnInit {

  isLogged = false;
  rolUsuario = '';
  today = new Date();
  isRolUsuario: boolean;
  isAdmin = false;
  roles: string[];

  constructor(private tokenService: TokenService, private router: Router) { }

  ngOnInit() {
    if (this.tokenService.getToken()) {
      this.isLogged = true;
      if(this.tokenService.getAuthorities().length >1){
        this.rolUsuario = "Gerente"
        this.isRolUsuario = false
        this.router.navigate(['/pages/escritorio']);
      }else{
        this.rolUsuario = "Recepcionista";
        this.isRolUsuario = true;
        this.router.navigate(['/pages/calendario']);
      }
    } else {
      this.isLogged = false;
      this.rolUsuario = '';
    }
    this.roles = this.tokenService.getAuthorities();
    this.roles.forEach(rol => {
      if (rol === 'ROLE_ADMIN') {
        this.isAdmin = true;
      }
    });
  }
  onLogOut(): void {
    this.tokenService.logOut();
    window.location.reload();
  }

}
