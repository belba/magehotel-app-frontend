import { Component, OnInit } from '@angular/core';
import { CheckInService } from '../check-in/check-in.service';
import { Reserva } from '../check-in/reserva';

@Component({
  selector: 'app-check-out',
  templateUrl: './check-out.component.html',
  styleUrls: ['./check-out.component.sass']
})
export class CheckOutComponent implements OnInit {

  reservas: Reserva[];
  constructor(private checkInService: CheckInService) { }

  ngOnInit(): void {
    this.checkInService.getReservas().subscribe(response => {
      this.reservas = response.filter(function (reserva) {
        return reserva.estado == 1 ;
      });
    })

  }


}
