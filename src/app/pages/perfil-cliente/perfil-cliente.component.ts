import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CheckInService } from '../check-in/check-in.service';
import { Reserva } from '../check-in/reserva';
import { ClienteService } from '../clientes/cliente.service';
import { Cliente } from '../clientes/clientes';

@Component({
  selector: 'app-perfil-cliente',
  templateUrl: './perfil-cliente.component.html',
  styleUrls: ['./perfil-cliente.component.sass']
})
export class PerfilClienteComponent implements OnInit {

  cliente: Cliente;
  reservasCliente: Reserva[];
  constructor(private clienteService: ClienteService, private checkInService: CheckInService, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe(params => {
      let id = +params.get('id');
      if (id) {
        this.clienteService.getCliente(id).subscribe( response => {
          this.cliente = response as Cliente;
        })
      }
      this.checkInService.getReservas().subscribe(response => {
        this.reservasCliente = response.filter(function (reserva) {
          return reserva.cliente.id == id ;
        });
      })


    });

  }
}
