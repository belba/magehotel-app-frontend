import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Reserva } from './reserva';

@Injectable()
export class CheckInService{
  private reservaURL: string = 'http://localhost:8181/api/reservas';
  constructor(private http: HttpClient){}

  public getReservas(): Observable<Reserva[]> {
    return this.http.get<Reserva[]>(this.reservaURL);
  }

  getReserva(id): Observable<Reserva>{
    return this.http.get<Reserva>(`${this.reservaURL}/${id}`);
  }

  updateReserva(reserva: Reserva): Observable<any> {
    return this.http.put<any>(`${this.reservaURL}/${reserva.id}`, reserva);
  }
}
