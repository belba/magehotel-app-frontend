import { Component, OnInit } from '@angular/core';
import { CheckInService } from './check-in.service';
import { Reserva } from './reserva';
import * as moment from 'moment'; // add this 1 of 4

@Component({
  selector: 'app-check-in',
  templateUrl: './check-in.component.html',
  styleUrls: ['./check-in.component.sass']
})
export class CheckInComponent implements OnInit {

  reservasPorEstadoFechaActual: Reserva[];
  reservasPorEstadoOtrasFechas: Reserva[];
  constructor(private checkInService: CheckInService) { }

  ngOnInit(): void {
    this.checkInService.getReservas().subscribe(response => {
      this.reservasPorEstadoFechaActual = response.filter(function (reserva) {
        return reserva.estado == 0 && reserva.fecha_inicio == moment(new Date(), "DD-MM-YYYY").format("YYYY-MM-DD").toLocaleString();
      });
    })

    this.checkInService.getReservas().subscribe(response => {
      this.reservasPorEstadoOtrasFechas = response.filter(function (reserva) {
        return reserva.estado == 0 && reserva.fecha_inicio != moment(new Date(), "DD-MM-YYYY").format("YYYY-MM-DD").toLocaleString();
      });
    })
  }

  fechaActual(): string {
    return moment(new Date(), "DD-MM-YYYY").format("YYYY-MM-DD").toLocaleString();
  }


}


