import { Data } from '@angular/router';
import { Cliente } from '../clientes/clientes';
import { Habitacion } from '../configuracion/habitaciones/Habitacion';

export class Reserva{
  id: number;
  habitacion: Habitacion;
  cliente: Cliente;
  fecha_inicio: string;
  fecha_final: string;
  estado: number;
}
