import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalConfig, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Categoria } from '../categorias/categoria';
import { CategoriaService } from '../categorias/categoria.service';
import { Piso } from '../nivel-habitaciones/nivel-habitaciones';
import { PisoService } from '../nivel-habitaciones/nivel-habitaciones.service';
import { Habitacion } from './Habitacion';
import { HabitacionService } from './habitacion.service';

@Component({
  selector: 'app-habitaciones',
  templateUrl: './habitaciones.component.html',
  styleUrls: ['./habitaciones.component.sass']
})
export class HabitacionesComponent implements OnInit {

  modal : NgbModalRef;
  habitaciones: Habitacion[];
  habitacion: Habitacion = new Habitacion();
  categorias: Categoria[];
  pisos: Piso[];

  estados =[
    {id: 1, nombre: "Disponible"},
    {id: 2, nombre: "Mantenimiento"},
    {id: 3, nombre: "Limpieza"},
    {id: 4, nombre: "Ocupado"}
  ]
  constructor(private habitacionService: HabitacionService,
    private pisoService: PisoService,
    private categoriaService: CategoriaService,
    private modalService: NgbModal,
    config: NgbModalConfig) {
    config.backdrop = 'static';
    config.keyboard = false;
   }

  ngOnInit(): void {
    this.habitacionService.getHabitaciones().subscribe(response => {
      this.habitaciones = response as Habitacion[];
    });
  }

  open(content, id) {
    this.modal = this.modalService.open(content, { centered: true, backdropClass: 'light-blue-backdrop' })
    this.modal.result.then((e) => {
    });

    this.pisoService.getPisos().subscribe(pisos => this.pisos = pisos);
    this.categoriaService.getCategorias().subscribe(categorias => this.categorias = categorias);

    if(id){
      this.habitacionService.getHabitacion(id).subscribe( response => {
        this.habitacion = response as Habitacion;
        console.log(response);

      });
    }else{
      this.habitacion = new Habitacion();
    }
  }

  compararPiso(o1: Piso, o2: Piso): boolean {
    if (o1 === undefined && o2 === undefined) {
      return true;
    }

    return o1 === null || o2 === null || o1 === undefined || o2 === undefined ? false : o1.id === o2.id;
  }

  compararCategoria(o1: Categoria, o2: Categoria): boolean {
    if (o1 === undefined && o2 === undefined) {
      return true;
    }

    return o1 === null || o2 === null || o1 === undefined || o2 === undefined ? false : o1.id === o2.id;
  }

  cerrar() {
    this.modal.close();
  }

  crear(): void{
    console.log(this.habitacion);
    this.habitacionService.crearHabitacion(this.habitacion).subscribe(
      habitacion => {
        this.ngOnInit();
        this.modal.close();
      });
  }

  update(): void{
    this.habitacionService.updateHabitacion(this.habitacion).subscribe(
      habitacion => {
        this.ngOnInit();
        this.modal.close();
      }
    )
  }

  eliminar(habitacion: Habitacion): void{
    this.habitacionService.eliminarHabitacion(habitacion.id).subscribe(
      () => {
        this.habitaciones = this.habitaciones.filter(hab => hab !== habitacion)
      }
    );
  }

}
