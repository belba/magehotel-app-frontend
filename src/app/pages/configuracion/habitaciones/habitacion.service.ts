import { HttpClient } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Habitacion } from './Habitacion';

@Injectable()
export class HabitacionService{
  private urlEndPoint: string = 'http://localhost:8181/api/habitaciones';

  constructor(private http: HttpClient){}

  getHabitaciones(): Observable<Habitacion[]>{

   return this.http.get(this.urlEndPoint).pipe(
      map((response: any) => {
        (response as Habitacion[]).map(habitacion => {
          return habitacion;
        });
        return response;
      }));
  }

  getHabitacion(id): Observable<Habitacion>{
    return this.http.get<Habitacion>(`${this.urlEndPoint}/${id}`);
  }

  crearHabitacion(habitacion: Habitacion): Observable<Habitacion>{
    return this.http.post(this.urlEndPoint, habitacion)
    .pipe(
      map((response: any ) => response.habitacion as Habitacion)
    );
  }

  updateHabitacion(habitacion: Habitacion): Observable<any> {
    return this.http.put<any>(`${this.urlEndPoint}/${habitacion.id}`, habitacion);
  }

  eliminarHabitacion(id: number): Observable<Habitacion>{
    return this.http.delete<Habitacion>(`${this.urlEndPoint}/${id}`);
  }

}
