import { Categoria } from '../categorias/categoria';
import { Piso } from '../nivel-habitaciones/nivel-habitaciones';

export class Habitacion{
  id: number;
  categoria: Categoria;
  piso: Piso;
  detalles: String;
  puerta: number;
  estado: number;
}
