import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Categoria } from './categoria';
import { CategoriaService } from './categoria.service';
import { NgbModal, NgbModalConfig, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-categorias',
  templateUrl: './categorias.component.html',
  styleUrls: ['./categorias.component.sass']
})
export class CategoriasComponent implements OnInit {

  modal : NgbModalRef;
  categorias: Categoria[];
  categoria: Categoria = new Categoria();
  constructor(private categoriaService: CategoriaService, private router: Router, private activatedRoute: ActivatedRoute, config: NgbModalConfig, private modalService: NgbModal) {
    config.backdrop = 'static';
    config.keyboard = false;
   }

  ngOnInit(): void {
    this.categoriaService.getCategorias().subscribe(response => {
      this.categorias = response as Categoria[];
    });
  }

  open(content, id) {
    this.modal = this.modalService.open(content, { centered: true, backdropClass: 'light-blue-backdrop' })
    this.modal.result.then((e) => {

    });

    if(id){
      this.categoriaService.getCategoria(id).subscribe((categoria) => this.categoria = categoria);
    }else{
      this.categoria = new Categoria();
    }
  }

  eliminar(categoria: Categoria): void{
    this.categoriaService.eliminarCategoria(categoria.id).subscribe(
      () => {
        this.categorias = this.categorias.filter(cat => cat !== categoria)
      }
    );
  }

  cerrar() {
    this.modal.close();
  }

  crear(): void{
    this.categoriaService.crearCategoria(this.categoria).subscribe(
      categoria => {
        this.ngOnInit();
        this.modal.close();
      });
  }
  update(): void{
    this.categoriaService.updateCategoria(this.categoria).subscribe(
      categoria => {
        this.ngOnInit();
        this.modal.close();
      }
    )
  }
}
