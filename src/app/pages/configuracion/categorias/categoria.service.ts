import { Injectable, Type } from "@angular/core";
import { HttpClient, HttpRequest, HttpEvent } from '@angular/common/http';
import { map, catchError, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Categoria } from './categoria';

@Injectable()
export class CategoriaService{
  private urlEndPoint: string = 'http://localhost:8181/api/categorias';

  constructor(private http: HttpClient){}

  getCategorias(): Observable<Categoria[]>{

    return this.http.get(this.urlEndPoint).pipe(
      map((response: any) => {
        (response as Categoria[]).map(categoria => {
          return categoria;
        });
        return response;
      }));
  }

  getCategoria(id): Observable<Categoria>{
    return this.http.get<Categoria>(`${this.urlEndPoint}/${id}`);
  }

  crearCategoria(categoria: Categoria): Observable<Categoria> {
    return this.http.post(this.urlEndPoint, categoria)
    .pipe(
      map((response: any ) => response.categoria as Categoria)
    );
  }

  eliminarCategoria(id: number): Observable<Categoria>{
    return this.http.delete<Categoria>(`${this.urlEndPoint}/${id}`);
  }

  updateCategoria(categoria: Categoria): Observable<any> {
    return this.http.put<any>(`${this.urlEndPoint}/${categoria.id}`, categoria);
  }

}
