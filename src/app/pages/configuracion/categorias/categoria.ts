export class Categoria{
  id: number;
  nombre: string;
  aforo: number;
  precio: number;
}
