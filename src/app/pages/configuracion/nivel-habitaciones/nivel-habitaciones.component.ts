import { Component, OnInit } from '@angular/core';
import { Piso } from './nivel-habitaciones';
import { PisoService } from './nivel-habitaciones.service';
import { NgbModal, NgbModalConfig, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-nivel-habitaciones',
  templateUrl: './nivel-habitaciones.component.html',
  styleUrls: ['./nivel-habitaciones.component.sass']
})
export class NivelHabitacionesComponent implements OnInit {

  modal : NgbModalRef;
  pisos: Piso[];
  piso: Piso = new Piso();
  constructor(private pisoService: PisoService, private router: Router, private activatedRoute: ActivatedRoute, config: NgbModalConfig, private modalService: NgbModal) {
    config.backdrop = 'static';
    config.keyboard = false;
  }

  ngOnInit(): void {
    this.pisoService.getPisos().subscribe(response => {
      this.pisos = response as Piso[];
    });
  }

  open(content, id) {
    this.modal = this.modalService.open(content, { centered: true, backdropClass: 'light-blue-backdrop' })
    this.modal.result.then((e) => {

    });

    if(id){
      this.pisoService.getPiso(id).subscribe((piso) => this.piso = piso);
    }else{
      this.piso = new Piso();
    }
  }

  eliminar(piso: Piso): void{
    this.pisoService.eliminarPiso(piso.id).subscribe(
      () => {
        this.pisos = this.pisos.filter(cat => cat !== piso)
      }
    );
  }

  cerrar() {
    this.modal.close();
  }

  crear(): void{
    this.pisoService.crearPiso(this.piso).subscribe(
      piso => {
        this.ngOnInit();
        this.modal.close();
      });
  }

  update(): void{
    this.pisoService.updatePiso(this.piso).subscribe(
      piso => {
        this.ngOnInit();
        this.modal.close();
      }
    )
  }

}
