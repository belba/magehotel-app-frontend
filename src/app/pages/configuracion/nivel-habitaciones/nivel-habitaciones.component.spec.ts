import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NivelHabitacionesComponent } from './nivel-habitaciones.component';

describe('NivelHabitacionesComponent', () => {
  let component: NivelHabitacionesComponent;
  let fixture: ComponentFixture<NivelHabitacionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NivelHabitacionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NivelHabitacionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
