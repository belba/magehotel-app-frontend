import { Injectable } from "@angular/core";
import { HttpClient, HttpRequest, HttpEvent } from '@angular/common/http';
import { map, catchError, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Piso } from './nivel-habitaciones';

@Injectable()
export class PisoService{
  private urlEndPoint: string = 'http://localhost:8181/api/pisos';

  constructor(private http: HttpClient){}

  getPisos(): Observable<Piso[]>{

    return this.http.get(this.urlEndPoint).pipe(
      tap((response: any) => {
        console.log('ClienteService: tap 1');
        (response as Piso[]).forEach(piso => console.log(piso.nombre));
      }),
      map((response: any) => {
        (response as Piso[]).map(piso => {
          return piso;
        });
        return response;
      }));
  }

  getPiso(id): Observable<Piso>{
    return this.http.get<Piso>(`${this.urlEndPoint}/${id}`);
  }

  crearPiso(piso: Piso): Observable<Piso> {
    return this.http.post(this.urlEndPoint, piso)
    .pipe(
      map((response: any ) => response.piso as Piso)
    );
  }

  eliminarPiso(id: number): Observable<Piso>{
    return this.http.delete<Piso>(`${this.urlEndPoint}/${id}`);
  }

  updatePiso(piso: Piso): Observable<any> {
    return this.http.put<any>(`${this.urlEndPoint}/${piso.id}`, piso);
  }

}
