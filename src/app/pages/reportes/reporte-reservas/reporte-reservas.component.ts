import { Component, OnInit } from '@angular/core';
import { CheckInService } from '../../check-in/check-in.service';
import { Reserva } from '../../check-in/reserva';

@Component({
  selector: 'app-reporte-reservas',
  templateUrl: './reporte-reservas.component.html',
  styleUrls: ['./reporte-reservas.component.sass']
})
export class ReporteReservasComponent implements OnInit {


  reservas: Reserva[];
  constructor(private checkInService: CheckInService) { }

  ngOnInit(): void {
    this.checkInService.getReservas().subscribe(response => {
      this.reservas = response as Reserva[];
    })
  }

}
