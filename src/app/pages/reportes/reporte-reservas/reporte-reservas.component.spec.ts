import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteReservasComponent } from './reporte-reservas.component';

describe('ReporteReservasComponent', () => {
  let component: ReporteReservasComponent;
  let fixture: ComponentFixture<ReporteReservasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReporteReservasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteReservasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
