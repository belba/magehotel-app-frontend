import { Component, OnInit } from '@angular/core';
import { NgbModal, NgbModalConfig, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { Habitacion } from '../configuracion/habitaciones/Habitacion';
import { HabitacionService } from '../configuracion/habitaciones/habitacion.service';
import { HabitacionesComponent } from '../configuracion/habitaciones/habitaciones.component';
import { Piso } from '../configuracion/nivel-habitaciones/nivel-habitaciones';
import { PisoService } from '../configuracion/nivel-habitaciones/nivel-habitaciones.service';

@Component({
  selector: 'app-recepcion',
  templateUrl: './recepcion.component.html',
  styleUrls: ['./recepcion.component.sass']
})
export class RecepcionComponent implements OnInit {

  modal : NgbModalRef;
  pisos: Piso[];
  habitaciones: Habitacion[];
  estadoHabitacion: Habitacion;
  constructor(private habitacionService: HabitacionService,
    private pisoService: PisoService, private modalService: NgbModal, config: NgbModalConfig) {
      config.backdrop = 'static';
      config.keyboard = false;
    }

  ngOnInit(): void {
    this.pisoService.getPisos().subscribe(pisos => this.pisos = pisos);
    //this.habitacionService.getHabitaciones().subscribe(habitaciones => this.habitaciones = habitaciones);

    this.habitacionService.getHabitaciones().subscribe(response => {
      this.habitaciones = response;

    });

  }
  open(content, habitacion: Habitacion) {
    this.modal = this.modalService.open(content, { centered: true, backdropClass: 'light-blue-backdrop' })
    this.modal.result.then((e) => {
    });
    this.estadoHabitacion = habitacion;
  }
  cerrar() {
    this.modal.close();
  }

  cambiarEstado(estado){
    console.log(this.estadoHabitacion);
    this.estadoHabitacion.estado = estado;
    this.habitacionService.updateHabitacion(this.estadoHabitacion).subscribe(response => {
      this.cerrar();
    });
  }
}
